// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TestingCharacter.generated.h"

UCLASS()
class UE4TUTORIALS_API ATestingCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATestingCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void InputJump(FKey Key);

	UFUNCTION()
	void InputMove_Forward(float AxisVal);

	UFUNCTION()
	void InputMove_Right(float AxisVal);

	UFUNCTION()
	void InputLookUp(float AxisVal);

	UFUNCTION()
	void InputTurn(float AxisVal);


private:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, meta = (AllowprivateAccess = "true"))
	class USpringArmComponent* SpringArm = nullptr;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, meta = (AllowprivateAccess = "true"))
	class UCameraComponent* Camera = nullptr;
};
