// Fill out your copyright notice in the Description page of Project Settings.

#include "TestingCharacter.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
ATestingCharacter::ATestingCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//UPROPERTY(BlueprintReadOnly, EditAnywhere, meta = (AllowprivateAccess = "true"))
	//	class USpringArmComponent* SpringArm = nullptr;

	//UPROPERTY(BlueprintReadOnly, EditAnywhere, meta = (AllowprivateAccess = "true"))
	//	class UCameraComponent* Camera = nullptr;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(GetRootComponent());

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
}

// Called when the game starts or when spawned
void ATestingCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATestingCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATestingCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &ATestingCharacter::InputJump);
	PlayerInputComponent->BindAxis("Move_Forward", this, &ATestingCharacter::InputMove_Forward);
	PlayerInputComponent->BindAxis("Move_Right", this, &ATestingCharacter::InputMove_Right);
	PlayerInputComponent->BindAxis("LookUp", this, &ATestingCharacter::InputLookUp);
	PlayerInputComponent->BindAxis("Turn", this, &ATestingCharacter::InputTurn);

}

void ATestingCharacter::InputJump(FKey Key)
{
	Jump();
}

void ATestingCharacter::InputMove_Forward(float AxisVal)
{
	//UCharacterMovementComponent* CharacterMovementComponent = GetCharacterMovement();
	//if (CharacterMovementComponent)
	//{
	//	FVector ForwardVector = GetActorForwardVector();
	//	CharacterMovementComponent->AddInputVector(ForwardVector*AxisVal);
	//}

	if ((Controller != NULL) && (AxisVal != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, AxisVal);
	}
}

void ATestingCharacter::InputMove_Right(float AxisVal)
{
	//UCharacterMovementComponent* CharacterMovementComponent = GetCharacterMovement();
	//if (CharacterMovementComponent)
	//{
	//	FVector RightVector = GetActorRightVector();
	//	CharacterMovementComponent->AddInputVector(RightVector*AxisVal);
	//}
	if ((Controller != NULL) && (AxisVal != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, AxisVal);
	}
}

void ATestingCharacter::InputLookUp(float AxisVal)
{
	AddControllerPitchInput(AxisVal);
}

void ATestingCharacter::InputTurn(float AxisVal)
{
	AddControllerYawInput(AxisVal);
}