// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UE4TutorialsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UE4TUTORIALS_API AUE4TutorialsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
